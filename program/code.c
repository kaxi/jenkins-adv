//SO2 IS1 222B LAB06
//Izaak Lakatorz
//li36103@zut.edu.pl


#define _XOPEN_SOURCE
#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>	
#include <crypt.h>

int main(int argc, char**argv)
{
	char* prefix = "$6$";
	int i;
	
	char* salt = malloc(sizeof(char) * strlen(prefix)+strlen(argv[2])+1);

	sprintf(salt, "%s%s",prefix,argv[2]);
	
	struct crypt_data data[1] = {0};
  	char *cr;

	cr = crypt_r(argv[1], salt, data);

	FILE *f = fopen("crypted.txt", "w");
    
    fprintf(f, "%s", cr);
	fclose(f);
	free(salt);
	return 0;
}
