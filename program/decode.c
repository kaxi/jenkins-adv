//SO2 IS1 210B LAB08
//Joanna Lewandowska
//jlewandowska@wi.zut.edu.pl



#define _XOPEN_SOURCE
#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>	
#include <crypt.h>
#include <fcntl.h>
#include <sys/types.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <time.h>
typedef int bool;
#define true 1
#define false 0

char* salt;
char* crypted;
unsigned char *f;
pthread_mutex_t mutex;
bool found = false;
int size;
bool normal = true;
double progress;
int linesInFile;
char* pass;

struct borders
{
    int start,end;
};

int countLines(char* file);
void* threadFunction(void* threadArguments);


int main(int argc, char**argv)
{

    if(argc < 3)
    {
       printf("Wrong arguments. Valid syntax ./executable hashFile fileToSearch [ threadsAmount]\n");
        return 1;
    }
    
    FILE* hashFile = fopen(argv[1],"r");
    if(hashFile == NULL)
    {
        printf("Unable to open hashFile. Program stopped\n");
        return 2;
    }
	size_t len = 0;
	int read = getline(&crypted, &len, hashFile);
    if(read == -1)
    {
     printf("Empty hashFile stoped program exectuion\n");
        return 3;
    }
	salt = malloc(sizeof(char)*20);
    int i=0, dollars=0, j;
	while( dollars <3) 
    {
	    if(crypted[i] == '$')
	    	dollars = dollars+1;
	    salt[i] =  crypted[i];
	    i=i+1;
    }
	fclose(hashFile);
    
  
    int threads = sysconf (_SC_NPROCESSORS_CONF);
    if(argc < 4)
    {
        normal = false;
        printf("Missing threads argurment, program will calculate time needed for searching in 1000 lines using 1-%d threads\n", threads); 
    }    
    else if(atoi(argv[3]) > threads)
    {
        printf("Wrong threads amount , program will continue for %d threads\n", threads); 
    }	
    else
    {
        printf("\nProgram will try to hack \n%s\n\n", crypted);
        threads = atoi(argv[3]);
    }
    pthread_t t[10];
    pthread_mutex_init(&mutex, NULL);
	int lines = countLines(argv[2]);    
    int linesForThread; 
    int searchedLines = 0;
    int left;
    struct borders b;    
    struct borders bTab[threads];
    struct timespec t1, t2;
   if(normal)
    {
        clock_gettime(CLOCK_MONOTONIC, &t1);
        linesForThread = lines/threads;
        if(lines % threads == 0)
        {
            for(i = 0; i< threads;i++)
            {
                b.start =  i*linesForThread;
                if( b.start != 0)
                    b.start = b.start - 1;
                b.end = b.start + linesForThread ;
                if(b.start == 0)
                    b.end -= 1;
                bTab[i] = b;                            
            }
       }       
       else
       {    
            for(i = 0; i< threads-1;i++)
            {
                b.start =  i*linesForThread;
                if( b.start != 0)
                    b.start = b.start - 1;
                b.end = b.start + linesForThread ;
                if(b.start == 0)
                    b.end -= 1;
                searchedLines += linesForThread;    
                bTab[i] = b;                            
            }
            left = lines - searchedLines;
            if(left >0)
            {
                b.start = searchedLines-1;
                b.end = lines;            
                bTab[i] = b; 
            }
       }
        
       for(i=0;i<threads;i++)
            pthread_create(&t[i],NULL,&threadFunction, &bTab[i]);
       
       for(i = 0; i< threads;i++)
            pthread_join(t[i],NULL);                     
       
        clock_gettime(CLOCK_MONOTONIC, &t2);
        if(found)
            printf("\nPassword hacked: %s", pass);
        else
            printf("Password not found\n");

        double timeStart = (double)t1.tv_sec + (double)t1.tv_nsec / 1000000000;
        double timeEnd = (double)t2.tv_sec + (double)t2.tv_nsec / 1000000000;
        double time = timeEnd-timeStart;
        printf("\nTime needed: %lfs\n", time);

 

        pthread_mutex_destroy(&mutex);
    }
    else
    {
        if(lines <1000)
            printf("Lines in file: %d, try with another one\n", lines);
        else
        {        
            for(int i=1; i<=threads; i++)
            {
                linesForThread = 1000/i;
                searchedLines = 0;            
                clock_gettime(CLOCK_MONOTONIC, &t1);
                if(1000 % threads == 0)
                {
                    for(j = 1; j <= i;j++)
                    {
                        b.start =  j*linesForThread;
                        if( b.start != 0)
                            b.start = b.start - 1;
                        
                        b.end = b.start + linesForThread ;
        
                        if(b.start == 0)
                            b.end -= 1;
                        
                        bTab[j-1] = b;                            
                    }
                }       
                else
                {    
                    for(j = 1; j <= i-1;j++)
                    {
                        b.start =  j*linesForThread;
                        if( b.start != 0)
                            b.start = b.start - 1;
                    
                        b.end = b.start + linesForThread ;

                        if(b.start == 0)
                            b.end -= 1;

                        searchedLines += linesForThread;                
                        bTab[j-1] = b;
                    }
                    left = lines - searchedLines;
                    if(left >0)
                    {
                        b.start = searchedLines;
                        b.end = lines;
                        bTab[j-1] = b; 
                    }
                }

                for(j=0;j<i;j++)
                    pthread_create(&t[j],NULL,&threadFunction, &bTab[j]);

                for(j = 0; j< i;j++)
                {
                    if(!found)
                        pthread_join(t[j],NULL);
                }

                clock_gettime(CLOCK_MONOTONIC, &t2);

 double timeStart = (double)t1.tv_sec + (double)t1.tv_nsec / 1000000000;
                double timeEnd = (double)t2.tv_sec + (double)t2.tv_nsec / 1000000000;
                double time = timeEnd-timeStart;

                printf("With %d threads in : %lfs\n", i, time);


            }

        }
        pthread_mutex_destroy(&mutex);
    }
    free(salt);	
	free(crypted);
    free(pass);
    return 0;
}

int countLines(char* file)
{
    int passwords = open(file,O_RDONLY);
    linesInFile = 0;
    struct stat s;
    int status = fstat(passwords, &s), i;
    size = s.st_size;
    f = (char *) mmap (0, size, PROT_READ, MAP_PRIVATE, passwords, 0);
	for(i=0;i<size;i++)
	{
        if(f[i] == '\n')
            linesInFile++;	
	}
    close(passwords);
    return linesInFile;
    
}

void* threadFunction(void* threadArguments)
{
    struct borders* b = (struct borders*) threadArguments; 
    int linesAmount = b->end - b->start;  

    char* lines = malloc(sizeof(char)* linesAmount *20);
    int i, l=0, c=0,j;
    char toCompare[256];
    char* res=NULL;
    int newLine = 0;    
    struct crypt_data data[1] = {0};
    
    for(i=0;i<size;i++)
    {     
        if(found == false)
        {
            if((newLine >= b->start) && (newLine != b->end))
            {
                pthread_mutex_lock(&mutex);   
                lines[l] = f[i];            
                l++;        
                pthread_mutex_unlock(&mutex);   
            }
            if(newLine == b->end)
                break;
      
            if(f[i] == '\n')
            {
                pthread_mutex_lock(&mutex);   
                newLine++;       
                pthread_mutex_unlock(&mutex);
            }        
        }    
    }   

    for(i=0;i<l;i++)
    {
        if(found == false)
        {   
            while(lines[i] != '\n')
            {
                pthread_mutex_lock(&mutex);   
                toCompare[c] = lines[i];
                c++;
                i++;
                pthread_mutex_unlock(&mutex);
            }
            res = crypt_r(toCompare,salt,data);
            pthread_mutex_lock(&mutex); 
            if(normal)
                printf("\r%.2f%% of file searched", (progress / linesInFile) * 100);
            progress = progress+1;
            pthread_mutex_unlock(&mutex);        
            if(!strcmp(crypted, res))
            {
                if(normal)
                {
                    pthread_mutex_lock(&mutex);
                    found = true;   
                    pass = malloc(sizeof(char)*c);  
                    for(i=0;i<c;i++)
                        pass[i] = toCompare[i];
                    pthread_mutex_unlock(&mutex); 
                }            
            }
           
            memset(&toCompare,0,c); 
            c=0;
        }

    }
    free(lines);        

}

