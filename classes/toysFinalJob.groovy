import com.here.demo.jobdsl.CustomStep
import com.here.demo.jobdsl.TemplateJobBuilder
import com.here.gradle.plugins.jobdsl.util.DslConfig
import javaposse.jobdsl.dsl.DslFactory

List dev_folders = ["Szkolenie_Basic/developer/Part02", "Szkolenie_Basic/developer2/Part02", "Szkolenie_Basic/developer3/Part02", "Szkolenie_Basic/developer4/Part02", "Szkolenie_Basic/developer5/Part02"]

dev_folders.each { folder ->

def job = new TemplateJobBuilder(this as DslFactory)
job.name = 'Toys_Final'
job.folders = [folder] 
job.matrixJob {
    steps {

        axes {
            text('FLAVOR', 'DEBUG', 'RELEASE')
        }
        CustomStep.echo(delegate, DslConfig.get('serverName'))

       // copyArtifacts('Szkolenie_Basic/Toy_prepare/FLAVOR=$FLAVOR') {
       // 	includePatterns('*.txt')
    }
    publishers {
        archiveArtifacts {
          pattern("""*.txt""".stripIndent())
        }
    }
}
job.build()
}