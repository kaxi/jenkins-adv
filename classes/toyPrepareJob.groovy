import com.here.demo.jobdsl.CustomStep
import com.here.demo.jobdsl.TemplateJobBuilder
import com.here.gradle.plugins.jobdsl.util.DslConfig
import javaposse.jobdsl.dsl.DslFactory



List dev_folders = ["Szkolenie_Basic/developer/Part02", "Szkolenie_Basic/developer2/Part02", "Szkolenie_Basic/developer3/Part02", "Szkolenie_Basic/developer4/Part02", "Szkolenie_Basic/developer5/Part02"]

dev_folders.each { folder ->
def job = new TemplateJobBuilder(this as DslFactory)
job.name = 'Toy_prepare'
job.folders = [folder] 
job.freeStyleJob {
    steps {
        CustomStep.echo(delegate, DslConfig.get('serverName'))

        shell("""touch toy.txt \\ 
                 echo This is job which prepares toys in /-put here variable-/ way. > toy.txt \\        	          	   
				  cat /var/lib/jenkins/szkolenie/toy_theme.txt >> toy.txt""".stripIndent())
    }
    publishers {
        archiveArtifacts {
          pattern('*.txt')
        }
    }
}
job.build()
}