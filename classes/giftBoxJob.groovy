import com.here.demo.jobdsl.CustomStep
import com.here.demo.jobdsl.TemplateJobBuilder
import com.here.gradle.plugins.jobdsl.util.DslConfig
import javaposse.jobdsl.dsl.DslFactory


List dev_folders = ["Szkolenie_Basic/developer/Part01", "Szkolenie_Basic/developer2/Part01", "Szkolenie_Basic/developer3/Part01", "Szkolenie_Basic/developer4/Part01", "Szkolenie_Basic/developer5/Part01"]

dev_folders.each { folder ->
    def job = new TemplateJobBuilder(this as DslFactory)
    job.name = 'GiftBox'
    job.folders = [folder]
    job.freeStyleJob {
        steps {
            CustomStep.echo(delegate, DslConfig.get('serverName'))

            copyArtifacts('Szkolenie_Basic/Cartoon box') {
            	includePatterns('*.txt')
            }
            copyArtifacts('Szkolenie_Basic/GiftBox') {
            	includePatterns('*.txt')
            }
            copyArtifacts('Szkolenie_Basic/GiftBox') {
            	includePatterns('*.txt')
            }
            shell("""bash /var/lib/jenkins/szkolenie/prepare.sh
                         cat /var/lib/jenkins/szkolenie/gift.txt""".stripIndent())
        }
        publishers {
            archiveArtifacts {
              pattern('*.txt')
                }
        }
    }
    job.build()
}