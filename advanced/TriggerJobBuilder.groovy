package com.here.jobdsl

import com.here.gradle.plugins.jobdsl.util.PipelineJobBuilder
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.jobs.MultiJob

class TriggerJobBuilder extends PipelineJobBuilder {
	TriggerJobBuilder(DslFactory dsl_factory,
		          String job_name) {
		super(dsl_factory)

		jobClass = MultiJob
		name = job_name

		addDsl {
			steps {
				phase('SCM') {
					phaseJob('SCM')
					continuationCondition('SUCCESSFUL')
				}
				phase('BUILD') {
					phaseJob('BUILD')
					continuationCondition('SUCCESSFUL')
				}
				phase('RUN-CODER') {
					phaseJob('RUN-CODER')
					continuationCondition('SUCCESSFUL')
				}
				phase('RUN-DECODER') {
					phaseJob('RUN-DECODER')
					continuationCondition('SUCCESSFUL')
				}
			}
		}
	}
}

