package com.here.jobdsl

import com.here.gradle.plugins.jobdsl.util.PipelineJobBuilder
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.jobs.MatrixJob

class BuildJobBuilder extends PipelineJobBuilder {
	BuildJobBuilder(DslFactory dsl_factory,
		      String job_name) {
		super(dsl_factory)

		jobClass = MatrixJob
		name = job_name

		addDsl {

			axes {
				text('FLAVOR', 'CODE', 'DECODE')
			}
			steps {
				copyArtifacts('SCM') {
					includePatterns('program/*')
					}

				shell("""
					if [ \"\$FLAVOR\" = \"CODE\" ] 
					then
					gcc -o code.out program/code.c -lcrypt
					elif [ \"\$FLAVOR\" = \"DECODE\" ] 
					then
					gcc -o decode.out program/decode.c -lcrypt -pthread
					fi
					""".stripIndent())
			}
			publishers {
				archiveArtifacts {
					pattern('*.out')
				}
			}
		}
	}
}

