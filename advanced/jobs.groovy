import com.here.jobdsl.*
import com.here.gradle.plugins.jobdsl.util.PipelineBuilder

List base_folder = ["Advanced"]

def pipelineBuilder = new PipelineBuilder()
pipelineBuilder.baseFolders(base_folder)

def scmJob = new SCMJobBuilder(this, 'SCM')

pipelineBuilder.addJob(scmJob)

def buildJob = new BuildJobBuilder(this, 'BUILD')

pipelineBuilder.addJob(buildJob)

def runCoderJob = new RunCoderJobBuilder(this, 'RUN-CODER')

pipelineBuilder.addJob(runCoderJob)

def runDecoderJob = new RunDecoderJobBuilder(this, 'RUN-DECODER')

pipelineBuilder.addJob(runDecoderJob)

def triggerJob = new TriggerJobBuilder(this, 'TRIGGER')

pipelineBuilder.addJob(triggerJob)

pipelineBuilder.build()
