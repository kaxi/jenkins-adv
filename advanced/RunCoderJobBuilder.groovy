package com.here.jobdsl

import com.here.gradle.plugins.jobdsl.util.PipelineJobBuilder
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.jobs.FreeStyleJob

class RunCoderJobBuilder extends PipelineJobBuilder {
	RunCoderJobBuilder(DslFactory dsl_factory,
		           String job_name) {
		super(dsl_factory)

		jobClass = FreeStyleJob
		name = job_name

		addDsl {
			parameters {
				stringParam('SALT', 'chleb', 'Salt to make it more difficult.')
			}
			steps {
				copyArtifacts('BUILD/FLAVOR=CODE') {
					includePatterns('*.out')
			}
				shell("""
					./code.out margaryna \"\$SALT\" 
					""".stripIndent())
			}
			publishers {
			 	archiveArtifacts {
			  		pattern('*.txt')
			     	}
		     	}
		}
	}
}

