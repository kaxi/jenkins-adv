package com.here.jobdsl

import com.here.gradle.plugins.jobdsl.util.PipelineJobBuilder
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.jobs.FreeStyleJob

class SCMJobBuilder extends PipelineJobBuilder {
	SCMJobBuilder(DslFactory dsl_factory,
		      String job_name) {
		super(dsl_factory)

		jobClass = FreeStyleJob
		name = job_name

		addDsl {
			scm {
				git {
					remote {
						name('origin')
	                                	url('https://bitbucket.org/kaxi/jenkins-adv.git')
					}
				}
			}
			steps {
				shell("ls")
			}
			publishers {
				archiveArtifacts {
					pattern('program/*')
				}
			}
		}
	}
}

