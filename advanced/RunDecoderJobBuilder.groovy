package com.here.jobdsl

import com.here.gradle.plugins.jobdsl.util.PipelineJobBuilder
import javaposse.jobdsl.dsl.DslFactory
import javaposse.jobdsl.dsl.jobs.FreeStyleJob

class RunDecoderJobBuilder extends PipelineJobBuilder {
	RunDecoderJobBuilder(DslFactory dsl_factory,
		           String job_name) {
		super(dsl_factory)

		jobClass = FreeStyleJob
		name = job_name

		addDsl {
			steps {
				copyArtifacts('SCM') {
					includePatterns('program/*.txt')
					}
				copyArtifacts('BUILD/FLAVOR=DECODE') {
					includePatterns('*.out')
					}
				copyArtifacts('RUN-CODER') {
					includePatterns('*.txt')
					}
				shell("""
					./decode.out crypted.txt program/dict.txt 1 
					""".stripIndent())
			}
		}
	}
}

